# Как проект устроен

Сверстан при помощи Bootstrap;
Для взаимодействий используются jQuery и Angular.


# Ход работы

— Для сборки проекта используется Grunt (склеивает js-файлы, оптимизирует изображения, компилирует sass — все это в Gruntfile.js). 

— Создан 12-колоночный шаблон страницы.

# Live preview

Здесь: http://university.netology.ru/user_data/vasilchenko/fbb1