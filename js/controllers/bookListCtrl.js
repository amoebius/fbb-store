'use strict';

/* booklist page controller */

controllers.controller('bookListCtrl', function ($scope, $http) {
  $scope.booksTotal = 0;
  $scope.show = function (number) {
    $scope.booksTotal = $scope.booksTotal + number;
    $scope.btnView = $scope.booksTotal < $scope.books.length;
  };

  $http.get(apiUrl + '/book/').
    success(function (data) {
      $scope.books = data;
      $scope.show(4);
    });
});
