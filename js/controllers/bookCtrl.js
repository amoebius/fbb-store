'use strict';
/* book page controller */

controllers.controller('bookCtrl', function ($scope, $routeParams, $http) {
  
  $http.get(apiUrl + '/book/' + $routeParams.id).
    success(function (data) {
      $scope.book = data;
      $scope.showEye = true;
    });

  $http.get(apiUrl + '/currency/').
    success(function (data) {
      $scope.currencyCode = data.find(function (currency) {
        return currency.ID === $scope.book.currency;
      }).CharCode;
    });

  document.addEventListener('mousemove', function(event) {
    eyeWatch(event.pageX, event.pageY);
  });

  var getElementCoords = function(element) {
    var rect = element.getBoundingClientRect();

    return {
      top: Math.round(rect.top + pageYOffset),
      left: Math.round(rect.left + pageXOffset),
      width: rect.width,
      height: rect.height
    };
  };

  var pupil = document.querySelector('.pupil');
  var eye = document.querySelector('.eye');

  var pupilPosX = 0;
  var pupilPosY = 0;
  
  var eyeBox = getElementCoords(eye);
  var centerPosX = eyeBox.left + eyeBox.width / 2;
  var centerPosY = eyeBox.top + eyeBox.height / 2;
  var radius = (eye.clientWidth - pupil.clientWidth) / 2;

  var eyeWatch = function(x, y) {
    var angle = Math.atan2(x - centerPosX, centerPosY - y);

    var borderPosX = Math.sin(angle) * radius;
    var borderPosY = Math.cos(angle) * radius;

    var relativePosX = x - centerPosX;
    var relativePosY = y - centerPosY;

    if(relativePosX < 0) { pupilPosX = Math.abs(borderPosX) * relativePosX / centerPosX; }
    else { pupilPosX = Math.abs(borderPosX) * relativePosX / (window.innerWidth - centerPosX); }

    if (relativePosY < 0) { pupilPosY = Math.abs(borderPosY) * relativePosY / (centerPosY - window.pageYOffset);} 
    else { pupilPosY = Math.abs(borderPosY) * relativePosY / (window.pageYOffset + window.innerHeight - centerPosY);}

    var length = Math.sqrt(Math.pow(pupilPosX, 2) + Math.pow(pupilPosY, 2));

    if (length < radius) {
      pupil.style.left = Math.round(pupilPosX + radius) + "px";
      pupil.style.top = Math.round(pupilPosY + radius) + "px";
    }
  };
});
