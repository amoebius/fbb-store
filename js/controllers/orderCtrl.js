'use strict';

/* book order controller */

controllers.controller('orderCtrl', function ($scope, $routeParams, $http) {
  $scope.customer = {};
  $scope.formData = {};
  $scope.deliveryPrice = 0;
  $scope.responseCodeOk = $scope.responseCodeBadReq = $scope.responseCodeIntErr = false;
  $scope.showAddress = false;
  $scope.showLoader = false;

  $scope.setDeliveryType = function (type) {
    $scope.deliveryPrice = type.price;
    $scope.showAddress = type.needAdress;
  };

  $scope.sendOrder = function () {
    
    $scope.showLoader = true;

    $scope.formData.manager = email;
    $scope.formData.book = $scope.book.id;
    $scope.formData.name = $scope.customer.name;
    $scope.formData.phone = $scope.customer.phone;
    $scope.formData.email = $scope.customer.email;
    $scope.formData.comment = $scope.customer.comment;
    $scope.formData.delivery = {id: $scope.customer.delivery};
    if ($scope.showAddress) {$scope.formData.delivery.address = $scope.customer.address; }
    $scope.formData.payment = {id: $scope.customer.payment, currency: $scope.book.currency};

    $http.post(apiUrl + '/order', $scope.formData).
      success(function (response, status) {
        if (status == 200) {
          $scope.responseCodeOk = true;
        }
        $scope.showLoader = false;
      }).
      error(function (response, status) {
        switch (status) {
          case 400: $scope.responseCodeBadReq = true; break;
          case 500: $scope.responseCodeIntErr = true; break;
        }
        $scope.responseMsg = response.message;
        $scope.showLoader = false;
      });
  };

  $http.get(apiUrl + '/book/' + $routeParams.id).
    success(function (data) {$scope.book = data;});

  $http.get(apiUrl + '/currency/').
    success(function (data) {
      $scope.currencyCode = data.find(function (cur) {
        return cur.ID === $scope.book.cur;
      }).CharCode;
    });

  $http.get(apiUrl + '/order/delivery').
    success(function (data) {
      $scope.deliveryList = data;
      $scope.customer.delivery = 'delivery-01';

      $scope.setDeliveryType($scope.deliveryList.find(function (type) {
        return type.id === $scope.customer.type;
      }));
    });

  $http.get(apiUrl + '/order/payment').
    success(function (data) {
      $scope.paymentList = data;
      $scope.customer.payment = 'payment-01';
    });
});