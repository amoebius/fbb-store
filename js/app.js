'use strict';
var apiUrl = 'https://netology-fbb-store-api.herokuapp.com';
var email = 'amoebiusss@gmail.com';
/*var partial = 'index.html/';*/
var controllers = angular.module('controllers', []);

/* routing */

var fbbApp = angular.module('fbbApp', ['ngRoute', 'ngDragDrop', 'ngSanitize', 'controllers']);

fbbApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/', { templateUrl: 'pages/booklist.html', controller: 'bookListCtrl' })
      .when('/books/:id', { templateUrl: 'pages/bookdetail.html', controller: 'bookCtrl' })
      .when('/order/:id', { templateUrl: 'pages/bookorder.html', controller: 'orderCtrl' })
      .when('/about', { templateUrl: 'pages/aboutus.html'})
      .otherwise({redirectTo: '/'});
  }]);

/*fbbApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', { templateUrl:'pages/book-list.html', controller: 'bookListCtrl'})
      .when('/books/:id', { templateUrl: 'pages/book-detail.html', controller: 'bookCtrl'})
      .when('/order/:id', { templateUrl: 'pages/order.html', controller: 'orderCtrl'})
      .when('/about', { templateUrl: 'pages/about.html'})
      .otherwise({redirectTo: '/'});
    // enable html5Mode for '#'-less URLs - still doesn't work :(
    /*$locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
  }]);*/