/*global module:false*/
module.exports = function(grunt) {
  grunt.initConfig({
   
    pkg: grunt.file.readJSON('package.json'),
    
    sass: {
         dist: {
         files: {
          'css/main.css': 'sass/styles.scss'
        }
      }
    },
    imagemin: {
      dynamic: {
        options: {
          optimizationLevel: 7,
          progressive: true
        },
        files: [{
          expand: true,
          cwd: 'imgsrc',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'img'
        }],
      }
    },

    connect: {
      test: {
        options: {
          port: 8000,
          base: '.'
        }
      }
    },
    
    autoprefixer: {
      styles: {
        src: 'css/*.css'
      }
    },

    watch: {
      less: {
        files: 'styles/**/*.sass',
        tasks: ['less']
      },
      autoprefixer: {
        files: 'css/*.css',
        tasks: ['autoprefixer']
      }
    },

    ftp_push: {
      demo: {
        options: {
          authKey: 'netology',
          host: 'university.netology.ru',
          dest: '/fbb1/',
          port: 21
        },
        files: [{
          expand: true,
          cwd: '.',
          src: [
            'index.html',
            'favicon.ico',
            'template.html',
            'css/*',
            'fonts/*',
            'pages/*',
            'img/*',
            'libs/*',
            'js/*',
            'js/controllers/**'
            
          ]
        }]
      }
    },
  });


  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-ftp-push');

  // Default task.
  grunt.registerTask('default', ['sass', 'autoprefixer', 'ftp_push']);
  grunt.registerTask('start', ['sass', 'autoprefixer', 'watch']);
  grunt.registerTask('demo', ['sass', 'autoprefixer', 'ftp_push']);
};